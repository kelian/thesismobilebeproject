(define (domain blocks)
(:types
  #object
)
(:constants
  b1 - #object
  b2 - #object
  b3 - #object
)
(:predicates
  (on ?x - #object ?y - #object)
  (ontable ?x - #object)
  (clear ?x - #object)
  (handempty)
  (holding ?x - #object)
)
(:durative-action pick-up_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (clear b1))
  (at start (ontable b1))
  (at start (handempty)))
:effect (and 
  (at end (not (ontable b1)))
  (at end (not (clear b1)))
  (at end (not (handempty)))
  (at end (holding b1)))
)
(:durative-action pick-up_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (clear b2))
  (at start (ontable b2))
  (at start (handempty)))
:effect (and 
  (at end (not (ontable b2)))
  (at end (not (clear b2)))
  (at end (not (handempty)))
  (at end (holding b2)))
)
(:durative-action pick-up_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (clear b3))
  (at start (ontable b3))
  (at start (handempty)))
:effect (and 
  (at end (not (ontable b3)))
  (at end (not (clear b3)))
  (at end (not (handempty)))
  (at end (holding b3)))
)
(:durative-action put-down_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition
  (at start (holding b1))
:effect (and 
  (at end (not (holding b1)))
  (at end (clear b1))
  (at end (handempty))
  (at end (ontable b1)))
)
(:durative-action stack_b1_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b1))
  (at start (clear b2)))
:effect (and 
  (at end (not (holding b1)))
  (at end (not (clear b2)))
  (at end (clear b1))
  (at end (handempty))
  (at end (on b1 b2)))
)
(:durative-action stack_b1_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b1))
  (at start (clear b3)))
:effect (and 
  (at end (not (holding b1)))
  (at end (not (clear b3)))
  (at end (clear b1))
  (at end (handempty))
  (at end (on b1 b3)))
)
(:durative-action put-down_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition
  (at start (holding b2))
:effect (and 
  (at end (not (holding b2)))
  (at end (clear b2))
  (at end (handempty))
  (at end (ontable b2)))
)
(:durative-action stack_b2_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b2))
  (at start (clear b1)))
:effect (and 
  (at end (not (holding b2)))
  (at end (not (clear b1)))
  (at end (clear b2))
  (at end (handempty))
  (at end (on b2 b1)))
)
(:durative-action stack_b2_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b2))
  (at start (clear b3)))
:effect (and 
  (at end (not (holding b2)))
  (at end (not (clear b3)))
  (at end (clear b2))
  (at end (handempty))
  (at end (on b2 b3)))
)
(:durative-action put-down_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition
  (at start (holding b3))
:effect (and 
  (at end (not (holding b3)))
  (at end (clear b3))
  (at end (handempty))
  (at end (ontable b3)))
)
(:durative-action stack_b3_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b3))
  (at start (clear b1)))
:effect (and 
  (at end (not (holding b3)))
  (at end (not (clear b1)))
  (at end (clear b3))
  (at end (handempty))
  (at end (on b3 b1)))
)
(:durative-action stack_b3_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (holding b3))
  (at start (clear b2)))
:effect (and 
  (at end (not (holding b3)))
  (at end (not (clear b2)))
  (at end (clear b3))
  (at end (handempty))
  (at end (on b3 b2)))
)
(:durative-action unstack_b1_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b1 b2))
  (at start (clear b1))
  (at start (handempty)))
:effect (and 
  (at end (holding b1))
  (at end (clear b2))
  (at end (not (clear b1)))
  (at end (not (handempty)))
  (at end (not (on b1 b2))))
)
(:durative-action unstack_b1_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b1 b3))
  (at start (clear b1))
  (at start (handempty)))
:effect (and 
  (at end (holding b1))
  (at end (clear b3))
  (at end (not (clear b1)))
  (at end (not (handempty)))
  (at end (not (on b1 b3))))
)
(:durative-action unstack_b2_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b2 b1))
  (at start (clear b2))
  (at start (handempty)))
:effect (and 
  (at end (holding b2))
  (at end (clear b1))
  (at end (not (clear b2)))
  (at end (not (handempty)))
  (at end (not (on b2 b1))))
)
(:durative-action unstack_b2_b3
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b2 b3))
  (at start (clear b2))
  (at start (handempty)))
:effect (and 
  (at end (holding b2))
  (at end (clear b3))
  (at end (not (clear b2)))
  (at end (not (handempty)))
  (at end (not (on b2 b3))))
)
(:durative-action unstack_b3_b1
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b3 b1))
  (at start (clear b3))
  (at start (handempty)))
:effect (and 
  (at end (holding b3))
  (at end (clear b1))
  (at end (not (clear b3)))
  (at end (not (handempty)))
  (at end (not (on b3 b1))))
)
(:durative-action unstack_b3_b2
:parameters ( )
:duration (= ?duration 0.002)
:condition (and 
  (at start (on b3 b2))
  (at start (clear b3))
  (at start (handempty)))
:effect (and 
  (at end (holding b3))
  (at end (clear b2))
  (at end (not (clear b3)))
  (at end (not (handempty)))
  (at end (not (on b3 b2))))
)
)
