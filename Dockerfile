FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/be-0.0.1-SNAPSHOT.jar be-0.0.1-SNAPSHOT.jar
COPY tflap /tflap
RUN apk update && apk add make && apk add g++ && mkdir files && cd tflap/src && make clean && make all
ENTRYPOINT ["java", "-jar", "/be-0.0.1-SNAPSHOT.jar"]

