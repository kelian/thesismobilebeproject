# Master Thesis Framework Backend (Process Planner)

Creates a user-specific BPMN plan based on the user's preferences and the database of application templates.

## Architecture
The backend consists of five modules

* src - Restful Web Service
* common - models used throughout the project
* PDDL Generator - created PDDL files
* tflap - automated planner
* BPMN Generator - creates BPMN files

The project is built with Docker. 

## Getting started (on Mac OS)

1. Checkout the source code
2. Build the project with command `./gradlew build`
3. Build the docker image with command `docker build -t be .`
4. Run the project with command `docker run -p 8080:8080 be`

## Adding new Application Template
The new Application Template needs to be added in the database JSON file `src/main/resources/files/database_apps.json`
and the backend project needs to be built again. 

The Application Template consists of four properties:

* id
* name
* problem
* actions

## References
### Initial code
The initial code of the PDDL and BPMN Generator was written in a different Java project. During the development it was decided to turn the project into a Spring Boot application and new project was created.

Old project: https://bitbucket.org/kelian/oldbethesisproject/src/master/ 

### TFLAP planner
The source code for TFLAP planner is from: https://bitbucket.org/ipc2018-temporal/team2/src/master/
