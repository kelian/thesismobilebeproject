package ee.ut.cs.se.mt.appgenerator.be;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class BeApplication {

	public static void main(String[] args) {
		run(BeApplication.class, args);
	}

}
