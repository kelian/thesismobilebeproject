package ee.ut.cs.se.mt.appgenerator.be.plan.model;

import java.util.List;

public class PlanRequest {

    private String appId;
    private List<String> requirements;
    private String metric;

    PlanRequest() {
    }

    PlanRequest(String appId, List<String> requirements, String metric) {
        this.appId = appId;
        this.requirements = requirements;
        this.metric = metric;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public List<String> getRequirements() {
        return requirements;
    }

    public void setRequirements(List<String> requirements) {
        this.requirements = requirements;
    }

    public String getMetric() {
        if (metric == null) {
            return "min_cost";
        } else {
            return metric;
        }
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }
}