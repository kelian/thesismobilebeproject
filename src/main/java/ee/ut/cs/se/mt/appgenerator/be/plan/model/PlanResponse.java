package ee.ut.cs.se.mt.appgenerator.be.plan.model;

public class PlanResponse {

    private String bpmnResourceName;
    private String bpmnFile;

    public PlanResponse(String bpmnResourceName, String bpmnFile) {
        this.bpmnResourceName = bpmnResourceName;
        this.bpmnFile = bpmnFile;
    }

    public String getBpmnFile() {
        return bpmnFile;
    }

    public void setBpmnFile(String bpmnFile) {
        this.bpmnFile = bpmnFile;
    }

    public String getBpmnResourceName() {
        return bpmnResourceName;
    }

    public void setBpmnResourceName(String bpmnResourceName) {
        this.bpmnResourceName = bpmnResourceName;
    }
}
