package ee.ut.cs.se.mt.appgenerator.be.plan;

import com.google.gson.Gson;
import ee.ut.cs.se.mt.appgenerator.be.plan.model.PlanRequest;
import ee.ut.cs.se.mt.appgenerator.be.plan.model.PlanResponse;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.BPMNGenerator;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;
import ee.ut.cs.se.mt.appgenerator.model.DatabaseData;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.PDDLGenerator;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;

@RestController
public class PlanController {

    public static final String DATABASE_FILE = "database_apps.json";

    private Gson gson = new Gson();

    @PostMapping(value = "/plan")
    public ResponseEntity getPlan(@RequestBody PlanRequest planRequest) {
        System.out.println(System.currentTimeMillis() + " - Main: Creating a new plan");
        String appId = planRequest.getAppId();

        try {
            ApplicationTemplate applicationTemplate = getAppData(appId, planRequest);
            System.out.println(System.currentTimeMillis() + " - Main: Read in Database file");

            String plannerOutput = new PDDLGenerator().create(applicationTemplate,
                    planRequest.getRequirements(),
                    planRequest.getMetric());

            String bpmnFile = new BPMNGenerator().create(plannerOutput, applicationTemplate);
            String bpmnResourceName = getPlanName(planRequest);

            PlanResponse response = new PlanResponse(bpmnResourceName, bpmnFile);

            System.out.println(System.currentTimeMillis() + " - Main: Sending BPMN to mobile");
            return ResponseEntity.ok(response);

        } catch (Exception e) {
            System.out.println(System.currentTimeMillis() + " - Main: Encountered an error: " + e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }

    public String getPlanName(PlanRequest planRequest) {
       StringBuilder sb = new StringBuilder();
       sb.append(planRequest.getAppId());

       for (String requirement: planRequest.getRequirements()) {
           sb.append("-").append(requirement);
       }

       sb.append("-").append(planRequest.getMetric()).append(".bpmn");
       return sb.toString();
    }

    public ApplicationTemplate getAppData(String appId, PlanRequest planRequest) throws Exception {
        String jsonFile = readInDatabaseFile();
        DatabaseData databaseData = gson.fromJson(jsonFile, DatabaseData.class);
        for (ApplicationTemplate app : databaseData.getApps()) {
            if (app.getId().equals(appId)) {
                app.setNameWithParameters(planRequest.getRequirements(), planRequest.getMetric());
                return app;
            }
        }

        throw new Exception("App with an id: " + appId + " cannot be found.");
    }

    private String readInDatabaseFile() {
        try {
            Resource resource = new ClassPathResource("files" + File.separator + DATABASE_FILE);
            InputStream inputStream = resource.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            reader.close();
            inputStream.close();
            return out.toString();
        } catch (Exception e) {
            return null;
        }
    }

}