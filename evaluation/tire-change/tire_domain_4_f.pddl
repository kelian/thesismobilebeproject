(define (domain tire_change_evaluation_domain)
    (:requirements :strips :typing :fluents :action-costs :durative-actions)

    (:predicates
        (no_conditions)
        (first_task_done)
        (second_task_done)
        (third_task_done)
        (tire_is_changed)
    )

    (:functions (total-cost) - number)

    (:durative-action first_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(no_conditions))
        :effect(and (at end(first_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action first_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(no_conditions))
        :effect(and (at end(first_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action second_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(first_task_done))
        :effect(and (at end(second_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action second_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(first_task_done))
        :effect(and (at end(second_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action third_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(second_task_done))
        :effect(and (at end(third_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action third_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(second_task_done))
        :effect(and (at end(third_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action fourth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(third_task_done))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 2)))
    )

    (:durative-action fourth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(third_task_done))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 1)))
    )

)