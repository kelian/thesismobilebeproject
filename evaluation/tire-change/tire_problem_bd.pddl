(define (problem tire_change_evaluation_problem)
    (:domain tire_change_evaluation_domain)

    (:init
        (= (total-cost) 0)
        (no_conditions)
    )

    (:goal
        (tire_is_changed)
    )

    (:metric minimize (total-time))

)