(define (domain tire_change_evaluation_domain)
    (:requirements :strips :typing :fluents :action-costs :durative-actions)

    (:predicates
        (no_conditions)
        (tire_is_changed)
    )

    (:functions (total-cost) - number)

    (:durative-action first_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(no_conditions))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 2)))
    )

    (:durative-action first_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(no_conditions))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 1)))
    )

)