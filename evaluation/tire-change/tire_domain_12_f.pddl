(define (domain tire_change_evaluation_domain)
    (:requirements :strips :typing :fluents :action-costs :durative-actions)

    (:predicates
        (no_conditions)
        (first_task_done)
        (second_task_done)
        (third_task_done)
        (fourth_task_done)
        (fifth_task_done)
        (sixth_task_done)
        (seventh_task_done)
        (eigth_task_done)
        (ninth_task_done)
        (tenth_task_done)
        (eleventh_task_done)
        (tire_is_changed)
    )

    (:functions (total-cost) - number)

    (:durative-action first_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(no_conditions))
        :effect(and (at end(first_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action first_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(no_conditions))
        :effect(and (at end(first_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action second_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(first_task_done))
        :effect(and (at end(second_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action second_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(first_task_done))
        :effect(and (at end(second_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action third_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(second_task_done))
        :effect(and (at end(third_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action third_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(second_task_done))
        :effect(and (at end(third_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action fourth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(third_task_done))
        :effect(and (at end(fourth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action fourth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(third_task_done))
        :effect(and (at end(fourth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action fifth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(fourth_task_done))
        :effect(and (at end(fifth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action fifth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(fourth_task_done))
        :effect(and (at end(fifth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action sixth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(fifth_task_done))
        :effect(and (at end(sixth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action sixth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(fifth_task_done))
        :effect(and (at end(sixth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action seventh_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(sixth_task_done))
        :effect(and (at end(seventh_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action seventh_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(sixth_task_done))
        :effect(and (at end(seventh_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action eigth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(seventh_task_done))
        :effect(and (at end(eigth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action eigth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(seventh_task_done))
        :effect(and (at end(eigth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action ninth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(eigth_task_done))
        :effect(and (at end(ninth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action ninth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(eigth_task_done))
        :effect(and (at end(ninth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action tenth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(ninth_task_done))
        :effect(and (at end(tenth_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action tenth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(ninth_task_done))
        :effect(and (at end(tenth_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action eleventh_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(tenth_task_done))
        :effect(and (at end(eleventh_task_done)) (at end(increase (total-cost) 2)))
    )

    (:durative-action eleventh_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(tenth_task_done))
        :effect(and (at end(eleventh_task_done)) (at end(increase (total-cost) 1)))
    )

    (:durative-action twelfth_task_cost
        :parameters()
        :duration(= ?duration 1)
        :condition(at start(eleventh_task_done))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 2)))
    )

    (:durative-action twelfth_task_dur
        :parameters()
        :duration(= ?duration 2)
        :condition(at start(eleventh_task_done))
        :effect(and (at end(tire_is_changed)) (at end(increase (total-cost) 1)))
    )

)