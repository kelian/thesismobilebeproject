package ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator;

import ee.ut.cs.se.mt.appgenerator.model.Action;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningTask;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningAction;

import java.util.*;

import static ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils.PlanningUtils.*;

public class DomainGenerator {

    private PlanningTask planningTask;

    public DomainGenerator() {}

    public PlanningTask create(PlanningTask definition, List<Action> actions) throws Exception {
        this.planningTask = definition;

        for (Action action : actions) {
            createPlanningAction(action);
        }

        return planningTask;
    }

    private void createPlanningAction(Action action) throws Exception {
        PlanningAction planningAction = new PlanningAction(action);

        analyzeConditions(planningAction, action.getConditions());
        analyzeEffects(planningAction, action.getEffects());

        formatActionItems(planningAction);
        planningTask.addPlanningAction(planningAction);
    }

    private void analyzeConditions(PlanningAction planningAction, List<String> conditionList) throws Exception {
        if (conditionList.isEmpty()) {
            conditionList.add(PREDICATE_NO_CONDITIONS);
            planningTask.addInit(PREDICATE_NO_CONDITIONS);
        }

        for (String condition : conditionList) {
            analyzeActionItem(planningAction, condition);
            planningAction.addCondition(condition);
        }
    }

    private void analyzeEffects(PlanningAction planningAction, List<String> effectList) throws Exception {
        for (String effect : effectList) {
            analyzeActionItem(planningAction, effect);
            planningAction.addEffect(effect);
        }
    }

    private void analyzeActionItem(PlanningAction planningAction, String item) throws Exception {
        List<String> parameterList = getParameters(item);

        getDomainType(planningAction.getName(), parameterList);
        getDomainPredicate(item, parameterList);
        getActionParameters(planningAction, parameterList);
    }

    private void getDomainType(String actionName, List<String> parameterList) throws Exception {
        for (String parameter : parameterList) {
            String object = getObject(parameter);
            if (object.equals(PARAMETER_ACTION)) {
                planningTask.addTypeAction(PARAMETER_ACTION, actionName);
            } else {
                planningTask.addTypeObject(object);
            }
        }
    }

    private void getDomainPredicate(String predicate, List<String> parameterList) throws Exception {
        StringBuilder predicateName = new StringBuilder(predicate.split(" ")[0]);

        Map<String, Integer> objectMap = new HashMap<>();
        for (String parameter : parameterList) {
            String object = getObject(parameter);
            if (objectMap.containsKey(object)) {
                int objectCount = objectMap.get(object);
                objectCount += 1;
                objectMap.put(object, objectCount);
            } else {
                objectMap.put(object, 1);
            }
        }

        for (String object : objectMap.keySet()) {
            int objectCount = objectMap.get(object);
            if (objectCount > 1) {
                for (int i = 1; i <= objectCount; i++) {
                    predicateName.append(" ?").append(object).append(i).append(" - ").append(object);
                }
            } else {
                predicateName.append(" ?").append(object).append(" - ").append(object);
            }
        }

        planningTask.addPredicate(predicateName.toString());
    }

    private void getActionParameters(PlanningAction planningAction, List<String> parameterList) throws Exception {
        for (String parameter : parameterList) {
            planningAction.addParameter(getActionParameter(parameter, planningAction.getName()));
        }
    }

    private void formatActionItems(PlanningAction planningAction) {
        List<String> formattedConditions = new ArrayList<>();
        for (String condition: planningAction.getConditions()) {
            formattedConditions.add(condition.replace("-", "_"));
        }
        planningAction.setConditions(formattedConditions);

        List<String> formattedEffects = new ArrayList<>();
        for (String effect : planningAction.getEffects()) {
            formattedEffects.add(effect.replace("-", "_"));
        }
        planningAction.setEffects(formattedEffects);

    }
}
