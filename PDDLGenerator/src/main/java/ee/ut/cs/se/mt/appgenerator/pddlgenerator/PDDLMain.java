package ee.ut.cs.se.mt.appgenerator.pddlgenerator;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;
import ee.ut.cs.se.mt.appgenerator.model.DatabaseData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

// Used for the PDDL Generator evaluation
public class PDDLMain {

    public static void main(String[] args) {
        try {
            String appId = "check_tire_pressure";

            List<String> requirements = new ArrayList<>();

            // String metric = "min_both";
            // String metric = "min_cost";
            String metric = "min_cost";

            ApplicationTemplate applicationTemplate = getAppData(appId, requirements, metric);

            new PDDLGenerator(true).create(applicationTemplate, requirements, metric);

            System.out.println("Success");
        } catch (Exception e) {
            System.out.println("Failure: " + e);
        }
    }

    private static ApplicationTemplate getAppData(String appId, List<String> requirements, String metric) throws Exception {
        JsonReader jsonReader = new JsonReader(new FileReader("src/main/resources/files/database_example.json"));
        DatabaseData databaseData = new Gson().fromJson(jsonReader, DatabaseData.class);
        for (ApplicationTemplate app : databaseData.getApps()) {
            if (app.getId().equals(appId)) {
                app.setNameWithParameters(requirements, metric);
                return app;
            }
        }

        throw new Exception("App with an id: " + appId + " cannot be found.");
    }
}
