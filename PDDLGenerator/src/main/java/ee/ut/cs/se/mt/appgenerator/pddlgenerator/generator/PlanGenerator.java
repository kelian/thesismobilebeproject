package ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PlanGenerator {

    private final static int MAXIMUM_WAITING_TIME = 5000; // 5 seconds

    public PlanGenerator() { }

    public String create(String domainFileContent, String problemFileContent) throws Exception {

        writeFile("domain.pddl", domainFileContent);
        writeFile("problem.pddl", problemFileContent);

        runAutomatedPlanner();

        String file = readInSolutionFile();

        if (file == null) {
            System.out.println(System.currentTimeMillis() + " - PDDL Generator: Didn't find the solution");
        }

        removePlanningFiles();

        return file;
    }

    private void writeFile(String fileName, String file) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("files", fileName)));
        if (file != null) {
            writer.write(file);
        } else {
            throw new Exception("Error while writing to file: " + fileName);
        }
        writer.close();
    }

    private void runAutomatedPlanner() {
        List<String> commands = new ArrayList<>();
        commands.add("tflap/src/tflap");
        commands.add("files/" + "domain.pddl");
        commands.add("files/" + "problem.pddl");
        commands.add("files/" + "solution.pddl");

        try {
            ProcessBuilder builder = new ProcessBuilder(commands);
            Process process = builder.start();
            process.waitFor(5, TimeUnit.SECONDS);  // let the process run for 5 seconds
            process.destroy();                     // tell the process to stop
            process.waitFor(10, TimeUnit.SECONDS); // give it a chance to stop
            process.destroyForcibly();             // tell the OS to kill the process
            process.waitFor();                     // the process is now dead

            printPlannerOutput(process);
        } catch (InterruptedException | IOException ignored) { }
    }

    private void printPlannerOutput(Process process) throws IOException {
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(process.getInputStream()));

        String s ;
        while ((s = stdInput.readLine()) != null)
        {
            System.out.println("PlanGenerator: " + s);
        }
    }

    private String readInSolutionFile() throws Exception {
        Thread.sleep(MAXIMUM_WAITING_TIME);

        boolean isLastPlan = false;
        int nrOfSolution = 0;

        while (!isLastPlan)  {
            int nrOfPlan = nrOfSolution + 1;

            File file = new File("files", "solution.pddl." + nrOfPlan);
            if (!file.exists()) {
                isLastPlan = true;
            } else {
                nrOfSolution += 1;
            }
        }

        if (nrOfSolution == 0) {
            return null;
        }

        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Found a solution");
        BufferedReader reader = new BufferedReader(new FileReader("files/solution.pddl." + nrOfSolution));
        StringBuilder builder = new StringBuilder();
        String currentLine = reader.readLine();
        while (currentLine != null) {
            builder.append(currentLine).append("\n");
            currentLine = reader.readLine();
        }

        String solution = builder.toString();
        System.out.println("PlanGenerator: Solution number " + nrOfSolution + " :\n" + solution );

        reader.close();

        removeSolutionFiles(nrOfSolution);

        return solution;

    }

    private void removeSolutionFiles(int nrOfSolutions) {
        for (int i = 1; i <= nrOfSolutions; i++) {
            File file = new File("files", "solution.pddl." + i);
            file.delete();
        }
    }

    private void removePlanningFiles() {
        File domainFile = new File("files", "domain.pddl.");
        domainFile.delete();
        File problemFile = new File("files", "problem.pddl.");
        problemFile.delete();
    }
}
