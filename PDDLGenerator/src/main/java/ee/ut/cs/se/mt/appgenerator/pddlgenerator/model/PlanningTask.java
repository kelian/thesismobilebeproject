package ee.ut.cs.se.mt.appgenerator.pddlgenerator.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlanningTask {

    private static final String TYPE_OBJECT = "object";

    private Map<String, List<String>> types = new HashMap<>();
    private List<String> predicates = new ArrayList<>();

    private Map<String, List<String>> objects = new HashMap<>();
    private List<String> init = new ArrayList<>();
    private List<String> goals = new ArrayList<>();

    private List<PlanningAction> planningActionList = new ArrayList<>();

    public PlanningTask() { }

    public Map<String, List<String>> getTypes() {
        return types;
    }

    public List<String> getPredicates() {
        return predicates;
    }

    public List<PlanningAction> getPlanningActionList() {
        return planningActionList;
    }

    public Map<String, List<String>> getObjects() {
        return objects;
    }

    public List<String> getInit() {
        return init;
    }

    public List<String> getGoals() {
        return goals;
    }

    public void addPredicate(String predicate) {
        // System.out.println("PlanningTask: Predicate: " + predicate);
        if (!predicates.contains(predicate)) {
            predicates.add(predicate);
        }
    }

    public void addType(String key, String type) {
        // System.out.println("PlanningTask: Add Type");
        // System.out.println("PlanningTask: Key: " + key);
        // System.out.println("PlanningTask: Type: " + type);

        if (!types.containsKey(key)) {
            types.put(key, new ArrayList<>());
        }

        List<String> typeList = types.get(key);

        if (!typeList.contains(type)) {
            typeList.add(type);
            types.put(key, typeList);
        }
    }

    public void addObject(String key, String name) {
        // System.out.println("PlanningTask: Add object");
        // System.out.println("PlanningTask: Key: " + key);
        // System.out.println("PlanningTask: Name: " + name);
        if (!objects.containsKey(key)) {
            objects.put(key, new ArrayList<>());
        }

        List<String> objectList = objects.get(key);

        if(!objectList.contains(name)) {
            objectList.add(name);
            objects.put(key, objectList);
        }
    }

    public void addInit(String init) {
        // System.out.println("PlanningTask: Init: " + init);
        if (!this.init.contains(init)) {
            this.init.add(init);
        }
    }

    public void addGoal(String goal) {
        // System.out.println("PlanningTask: Goal: " + goal);
        if (!goals.contains(goal)) {
            goals.add(goal);
        }
    }

    public void addTypeAction(String key, String actionName) {
        addType(key, key + "_" + actionName);
    }

    public void addTypeObject(String type) {
        addType(TYPE_OBJECT, type);
    }

    public void addPlanningAction(PlanningAction planningAction) {
        planningActionList.add(planningAction);
    }
}
