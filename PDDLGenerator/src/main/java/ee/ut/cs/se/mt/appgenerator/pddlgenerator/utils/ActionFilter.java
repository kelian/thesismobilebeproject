package ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils;

import ee.ut.cs.se.mt.appgenerator.model.Action;

import java.util.ArrayList;
import java.util.List;

public class ActionFilter {

    public ActionFilter() { }

    public List<Action> create(List<Action> actions, List<String> requirements, Boolean shouldFilter) {
        List<Action> filteredActions = new ArrayList<>();

        for (Action action : actions) {
            boolean addAction = true;
            for (String actionRequirement: action.getRequirements()) {
                if (!requirements.contains(actionRequirement) && shouldFilter) {
                    addAction = false;
                    break;
                }
            }

            if (addAction) {
                filteredActions.add(action);
            }
        }

        return filteredActions;
    }
}
