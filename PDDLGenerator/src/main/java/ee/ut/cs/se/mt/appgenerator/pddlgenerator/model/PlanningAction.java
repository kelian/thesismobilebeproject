package ee.ut.cs.se.mt.appgenerator.pddlgenerator.model;

import ee.ut.cs.se.mt.appgenerator.model.Action;

import java.util.ArrayList;
import java.util.List;

public class PlanningAction {

    private String name;
    private int duration;
    private int totalCost;
    private List<String> parameters = new ArrayList<>();
    private List<String> conditions = new ArrayList<>();
    private List<String> effects = new ArrayList<>();

    public PlanningAction(Action action) {
        this.name = action.getName();
        this.duration = action.getDuration();
        this.totalCost = action.getTotalCost();
    }

    public PlanningAction(String name, int duration, List<String> parameters, List<String> conditions, List<String> effects) {
        this.name = name;
        this.duration = duration;
        this.parameters = parameters;
        this.conditions = conditions;
        this.effects = effects;
    }

    public void addParameter(String parameter) {
        String[] parameterSplit = parameter.split("-");

        boolean hasParameter = false;
        for (String listParameter: parameters) {
            if (listParameter.startsWith(parameterSplit[0])) {
                hasParameter = true;
                break;
            }
        }

        if (!hasParameter) {
            parameters.add(parameter);
        }
    }

    public void addCondition(String condition) {
        conditions.add(condition);
    }

    public void addEffect(String effect) {
        effects.add(effect);
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public void setEffects(List<String> effects) {
        this.effects = effects;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public List<String> getEffects() {
        return effects;
    }

    public int getTotalCost() {
        return totalCost;
    }
}