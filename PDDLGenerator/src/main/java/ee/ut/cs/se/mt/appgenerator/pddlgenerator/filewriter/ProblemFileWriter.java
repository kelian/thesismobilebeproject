package ee.ut.cs.se.mt.appgenerator.pddlgenerator.filewriter;

import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningTask;

import java.util.List;
import java.util.Map;

public class ProblemFileWriter {

    private StringBuilder file = new StringBuilder();

    public ProblemFileWriter() { }

    public String write(String appId, PlanningTask definition, String metric) {
        addStart(appId);
        addObjects(definition);
        addInit(definition);
        addGoals(definition);
        addMetric(metric);
        addEnd();

        return file.toString();
    }

    private void addStart(String appId) {
        file.append("(define (problem ").append(appId).append("_problem)").append(getEnd());
        file.append(getTab());
        file.append("(:domain ").append(appId).append("_domain)").append(getEnd());
        file.append(getEnd());
    }

    private void addObjects(PlanningTask definition) {
        if (!definition.getObjects().isEmpty()) {
            file.append(getTab());
            file.append("(:objects").append(getEnd());

            addObjectsFromGenerator(definition.getObjects());

            file.append(getTab());
            file.append(")").append(getEnd());
            file.append(getEnd());
        }
    }

    private void addObjectsFromGenerator(Map<String, List<String>> objects) {
        if(!objects.isEmpty()) {
            for (String key: objects.keySet()) {
                file.append(getTwoTabs());

                for (String value : objects.get(key)) {
                    file.append(value).append(" ");
                }

                file.append("- ").append(key).append(getEnd());
            }
        }
    }

    private void addInit(PlanningTask definition) {
        file.append(getTab());
        file.append("(:init").append(getEnd());

        addTotalCost();
        addInitFromGenerator(definition.getInit());

        file.append(getTab());
        file.append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addTotalCost() {
        file.append(getTwoTabs());
        file.append("(= (total-cost) 0)");
        file.append(getEnd());
    }

    private void addInitFromGenerator(List<String> initList) {
        for (String init: initList) {
            file.append(getTwoTabs());
            file.append("(");
            file.append(init);
            file.append(")").append(getEnd());
        }
    }

    private void addGoals(PlanningTask definition) {
        file.append(getTab());
        file.append("(:goal").append(getEnd());
        file.append(getTwoTabs()).append("(");

        createGoalsFromList(definition.getGoals());

        file.append(")").append(getEnd());
        file.append(getTab());
        file.append(")").append(getEnd());
        file.append(getEnd());
    }


    private void createGoalsFromList(List<String> goalList) {
        if (goalList.size() > 1) {
            addGoalWithAnd(goalList);
        } else if (goalList.size() > 0) {
            file.append(goalList.get(0));
        }
    }

    private void addGoalWithAnd(List<String> goalList) {
        file.append("and ");

        for (String goal : goalList) {
            file.append("(");
            file.append(goal);
            file.append(") ");
        }

        file.deleteCharAt(file.length() - 1);
    }

    private void addMetric(String metric) {
        file.append(getTab());
        file.append("(:metric minimize ").append(getMetric(metric)).append(")").append(getEnd());
        file.append(getEnd());
    }

    private String getMetric(String metric) {
        if (metric == null || metric.equals("min_cost")) {
            return "(total-cost)";
        } else if (metric.equals("min_both")) {
            return "(+ (total-cost) (total-time))";
        } else {
            return "(total-time)";
        }
    }

    private void addEnd() {
        file.append(")");
    }

    private String getTab() {
        return "    ";
    }

    private String getTwoTabs() {
        return "        ";
    }

    private String getEnd() {
        return "\n";
    }
}
