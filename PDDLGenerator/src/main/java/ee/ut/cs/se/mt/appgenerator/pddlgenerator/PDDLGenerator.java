package ee.ut.cs.se.mt.appgenerator.pddlgenerator;

import ee.ut.cs.se.mt.appgenerator.model.Action;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.filewriter.DomainFileWriter;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.filewriter.ProblemFileWriter;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator.DomainGenerator;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator.PlanGenerator;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator.ProblemGenerator;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningTask;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils.ActionFilter;

import java.util.List;

public class PDDLGenerator {

    private Boolean shouldFilter = true;

    public PDDLGenerator() { }

    // For PDDL Generator filtering evaluation only
    public PDDLGenerator(Boolean shouldFilter) {
        this.shouldFilter = shouldFilter;
    }

    public String create(ApplicationTemplate applicationTemplate, List<String> preferences, String metric) throws Exception {

        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Start");

        List<Action> filteredActions = new ActionFilter().create(applicationTemplate.getActions(), preferences, shouldFilter);
        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Actions are filtered");

        PlanningTask planningTask = new PlanningTask();
        planningTask = new DomainGenerator().create(planningTask, filteredActions);
        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Planning Domain is generated");

        planningTask = new ProblemGenerator().create(planningTask, applicationTemplate.getProblem());
        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Planning Problem is generated");

        String domainFileContent = new DomainFileWriter().write(applicationTemplate.getId(), planningTask);
        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Domain File is written");
        System.out.println("PDDLGenerator: Domain File: \n" + domainFileContent);

        System.out.println(" ");

        String problemFileContent = new ProblemFileWriter().write(applicationTemplate.getId(), planningTask, metric);
        System.out.println(System.currentTimeMillis() + " - PDDL Generator: Problem File is written");
        System.out.println("PDDLGenerator: Problem File: \n" + problemFileContent);

        return new PlanGenerator().create(domainFileContent, problemFileContent);
    }
}
