package ee.ut.cs.se.mt.appgenerator.pddlgenerator.generator;

import ee.ut.cs.se.mt.appgenerator.model.Problem;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningTask;

public class ProblemGenerator {

    private PlanningTask planningTask;

    public ProblemGenerator() { }

    public PlanningTask create(PlanningTask planningTask, Problem problem) {
        this.planningTask = planningTask;

        createObjectsFromProblem(problem);
        createInitFromProblem(problem);
        createGoalsFromProblem(problem);
        return planningTask;
    }

    private void createObjectsFromProblem(Problem problem) {
        for (String key: problem.getObjects().keySet()) {
            for (String value: problem.getObjects().get(key)) {
                planningTask.addObject(key, value);
            }
        }
    }

    private void createInitFromProblem(Problem problem) {
        for (String init: problem.getInit()) {
            planningTask.addInit(init);
        }
    }

    private void createGoalsFromProblem(Problem problem) {
        for (String goal: problem.getGoal()) {
            planningTask.addGoal(goal);
        }
    }
}
