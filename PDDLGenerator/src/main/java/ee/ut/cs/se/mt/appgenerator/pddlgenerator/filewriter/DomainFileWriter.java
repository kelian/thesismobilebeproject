package ee.ut.cs.se.mt.appgenerator.pddlgenerator.filewriter;

import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningAction;
import ee.ut.cs.se.mt.appgenerator.pddlgenerator.model.PlanningTask;

import java.util.List;
import java.util.Map;

import static ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils.Constants.DOMAIN_REQUIREMENTS;

public class DomainFileWriter {

    private StringBuilder file = new StringBuilder();

    public DomainFileWriter() { }

    public String write(String appId, PlanningTask definition) {
        addStart(appId);
        addTypes(definition.getTypes());
        addPredicates(definition.getPredicates());
        addFunctions();
        addActions(definition.getPlanningActionList());
        addEnd();

        return file.toString();
    }

    private void addStart(String appId) {
        file.append("(define (domain ").append(appId).append("_domain)").append(getEnd());
        file.append(getTab());
        file.append("(:requirements ").append(DOMAIN_REQUIREMENTS).append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addTypes(Map<String, List<String>> types) {
        if (!types.isEmpty()) {
            file.append(getTab());
            file.append("(:types");

            boolean firstRow = true;

            for (String key : types.keySet()) {
                if (firstRow) {
                    firstRow = false;
                } else {
                    file.append("           ");
                }

                for (String value : types.get(key)) {
                    file.append(" ").append(value);
                }

                file.append(" - ").append(key).append(getEnd());
            }

            file.append(getTab()).append(")").append(getEnd());
            file.append(getEnd());
        }
    }

    private void addPredicates(List<String> predicates) {
        file.append(getTab());
        file.append("(:predicates").append(getEnd());

        for (String predicate : predicates) {
            file.append(getTwoTabs());
            file.append("(").append(predicate).append(")\n");
        }

        file.append(getTab()).append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addFunctions() {
        file.append(getTab());
        file.append("(:functions (total-cost) - number)").append(getEnd());
        file.append(getEnd());
    }

    private void addActions(List<PlanningAction> actionList) {
        for (PlanningAction planningAction : actionList) {
            addAction(planningAction);
            file.append(getEnd());
        }
    }

    private void addAction(PlanningAction planningAction) {
        file.append(getTab());
        file.append("(:durative-action ").append(planningAction.getName()).append(getEnd());

        addParameters(planningAction.getParameters());
        addDuration(planningAction.getDuration());
        addConditions(planningAction.getConditions());
        addEffects(planningAction.getEffects(), planningAction.getTotalCost());

        file.append(getTab());
        file.append(")").append(getEnd());
    }

    private void addParameters(List<String> parameters) {
        file.append(getTwoTabs());
        file.append(":parameters(");

        for (String parameter : parameters)
            file.append(parameter).append(" ");

        if (parameters.size() > 0)
            file.deleteCharAt(file.length() - 1);

        file.append(")").append(getEnd());
    }

    private void addDuration(int duration) {
        file.append(getTwoTabs());
        file.append(":duration(= ?duration ").append(duration);
        file.append(")").append(getEnd());
    }

    private void addConditions(List<String> conditions) {
        file.append(getTwoTabs());
        file.append(":condition(");
        addActionItems(conditions, true);
        file.append(")").append(getEnd());
    }

    private void addEffects(List<String> effects, int totalCost) {
        file.append(getTwoTabs());
        file.append(":effect(");

        effects.add("increase (total-cost) " + totalCost);

        addActionItems(effects, false);
        file.append(")").append(getEnd());
    }

    private void addActionItems(List<String> itemList, boolean isAtStart) {
        if (itemList.size() > 1) {
            addActionItemsWithAnd(itemList, isAtStart);
        } else if (itemList.size() > 0) {
            addActionItem(itemList.get(0), isAtStart);
        }
    }

    private void addActionItemsWithAnd(List<String> itemList, boolean isAtStart) {
        file.append("and ");

        for (String item : itemList) {
            file.append("(");
            addActionItem(item, isAtStart);
            file.append(") ");
        }

        file.deleteCharAt(file.length() - 1);
    }

    private void addActionItem(String item, boolean isAtStart) {
        if (isAtStart) {
            file.append("at start(");
        } else {
            file.append("at end(");
        }
        file.append(item).append(")");
    }
    private void addEnd() {
        file.append(")");
    }

    private String getTab() {
        return "    ";
    }

    private String getTwoTabs() {
        return "        ";
    }

    private String getEnd() {
        return "\n";
    }
}
