package ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils;

public class Constants {
    public static final String DOMAIN_REQUIREMENTS = ":strips :typing :fluents :action-costs :durative-actions";
}
