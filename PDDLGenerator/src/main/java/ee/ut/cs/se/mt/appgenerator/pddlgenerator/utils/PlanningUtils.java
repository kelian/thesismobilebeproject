package ee.ut.cs.se.mt.appgenerator.pddlgenerator.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PlanningUtils {

    public static final String PARAMETER_ACTION = "action";

    public static final String PREDICATE_NO_CONDITIONS = "no_conditions";

    public static List<String> getParameters(String item) {
        String[] itemArray = item.split(" ");
        if (itemArray.length > 1)
            return Arrays.asList(itemArray).subList(1, itemArray.length);
        return new ArrayList<>();
    }

    public static String getObject(String parameter) throws Exception {
        String[] parameterArray = parameter.split("-");
        if (parameterArray.length != 0) {
            return parameterArray[0].substring(1);
        } else {
            throw new Exception("Error while parsing conditions and effects");
        }
    }

    public static String getActionParameter(String parameter, String actionName) throws Exception {
        return getFormattedParameter(parameter) + " - " + getParameterObject(parameter, actionName);
    }

    private static String getParameterObject(String parameter, String actionName) throws Exception {
        String object = getObject(parameter);
        if (object.equals(PARAMETER_ACTION)) {
            return PARAMETER_ACTION + "_" + actionName;
        } else {
            return object;
        }
    }

    private static String getFormattedParameter(String parameter) {
        return parameter.replace("-", "_");
    }
}
