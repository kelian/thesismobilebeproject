package ee.ut.cs.se.mt.appgenerator.model;

import java.util.List;
import java.util.Map;

public class Problem {
    private Map<String, List<String>> objects;
    private List<String> init;
    private List<String> goal;

    public Map<String, List<String>> getObjects() {
        return objects;
    }

    public List<String> getInit() {
        return init;
    }

    public List<String> getGoal() {
        return goal;
    }
}