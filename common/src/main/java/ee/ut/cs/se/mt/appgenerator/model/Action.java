package ee.ut.cs.se.mt.appgenerator.model;

import java.util.ArrayList;
import java.util.List;

public class Action {

    private String name;
    private int duration;
    private List<String> conditions = new ArrayList<>();
    private List<String> effects = new ArrayList<>();
    private int totalCost;
    private List<String> requirements = new ArrayList<>();
    private String bpmn = null;

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public List<String> getEffects() {
        return effects;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public String getBpmn() {
        return bpmn;
    }

    public List<String> getRequirements() {
        return requirements;
    }

    public void addConditions(List<String> conditions) {
        this.conditions.addAll(conditions);
    }

    public void addEffects(List<String> effects) {
        this.effects.addAll(effects);
    }

    public boolean hasBpmn() {
        return bpmn != null && !bpmn.isEmpty();
    }

}