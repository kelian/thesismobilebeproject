package ee.ut.cs.se.mt.appgenerator.model;

import java.util.List;

public class ApplicationTemplate {

    private String id;
    private String name;
    private Problem problem;
    private String nameWithRequirements;
    private String idWithRequirements;

    public ApplicationTemplate() {

    }

    private List<Action> actions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public void setNameWithParameters(List<String> requirements, String metric) {
        idWithRequirements = id;
        nameWithRequirements = name;

        for (String requirement: requirements) {
            nameWithRequirements += " " + requirement;
            idWithRequirements += "_" + requirement;
        }

        nameWithRequirements += " " + metric;
        idWithRequirements += "_" + metric;
    }

    public String getNameWithRequirements() {
        return nameWithRequirements;
    }

    public String getIdWithRequirements() {
        return idWithRequirements;
    }
}
