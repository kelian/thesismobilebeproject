package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model;

import java.util.HashMap;
import java.util.Map;

public class BPMNDefinitions {

    private Map<String, String> namespaceList = new HashMap<>();
    private Map<String, String> attributes = new HashMap<>();

    public void addAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void addNameSpace(String key, String value) {
        namespaceList.put(key, value);
    }

    public Map<String, String> getNamespaceList() {
        return namespaceList;
    }
}
