package ee.ut.cs.se.mt.appgenerator.bpmngenerator.parser;

import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMN;
import ee.ut.cs.se.mt.appgenerator.model.Action;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;

import java.util.*;

public class PlanActionsBPMNParser {

    public PlanActionsBPMNParser() {
    }

    public Map<Float, List<BPMN>> getActionsBPMN(ApplicationTemplate applicationTemplate, Map<Float, List<String>>  pddlPlan) throws Exception {
        Map<Float, List<BPMN>> bpmnList = new TreeMap<>();

        BPMNParser dataParser = new BPMNParser();

        for (Float step : pddlPlan.keySet()) {
            for (String planAction : pddlPlan.get(step)) {
                String planActionName = planAction.split(" ")[0];

                Action action = getActionFromDatabase(applicationTemplate, planActionName);
                if (action != null && action.hasBpmn()) {

                    if (!bpmnList.containsKey(step)) {
                        bpmnList.put(step, new ArrayList<>());
                    }
                    List<BPMN> list = bpmnList.get(step);
                    list.add(dataParser.getBPMN(action.getBpmn()));
                    bpmnList.put(step, list);
                }
            }
        }

        return bpmnList;
    }

    private Action getActionFromDatabase(ApplicationTemplate applicationTemplate, String name) {
        for (Action action : applicationTemplate.getActions()) {
            if (action.getName().equals(name)) {
                return action;
            }
        }
        return null;
    }
}
