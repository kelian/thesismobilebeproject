package ee.ut.cs.se.mt.appgenerator.bpmngenerator;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;
import ee.ut.cs.se.mt.appgenerator.model.DatabaseData;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class BPMNMain {

    public static void main(String[] args) {
        String plannerOutput =
                "0.002: (park_car audi) [1.000]\n" +
                        "1.010: (check_tire_pressure_manually) [4.000]\n" +
                        "1.010: (check_tire_pressure_bluetooth) [4.000]\n" +
                        ";Makespan: 5\n" +
                        ";Actions:  2\n" +
                        ";Planning time: 0.11\n" +
                        ";Total time: 0.11\n" +
                        ";2 expanded nodes\n";

        List<String> requirements = new ArrayList<>();
        String metric = "min_cost";

        try {
            String solution = new BPMNGenerator().create(plannerOutput, getAppData("check_tire_pressure", requirements, metric));
            System.out.println("Success: \n");
            System.out.println(solution);
        } catch (Exception e) {
            System.out.println("Failure: " + e);
        }
    }

    private static ApplicationTemplate getAppData(String appId, List<String> requirements, String metric) throws Exception {
        JsonReader jsonReader = new JsonReader(new FileReader("src/main/resources/files/database_apps.json"));
        DatabaseData databaseData = new Gson().fromJson(jsonReader, DatabaseData.class);
        for (ApplicationTemplate app : databaseData.getApps()) {
            if (app.getId().equals(appId)) {
                app.setNameWithParameters(requirements, metric);
                return app;
            }
        }

        throw new Exception("App with an id: " + appId + " cannot be found.");
    }
}
