package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.diagram;

public class BPMNLabel {

    private Bounds bounds;

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Bounds getBounds() {
        return bounds;
    }
}
