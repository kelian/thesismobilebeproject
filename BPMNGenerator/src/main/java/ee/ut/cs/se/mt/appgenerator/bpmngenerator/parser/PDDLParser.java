package ee.ut.cs.se.mt.appgenerator.bpmngenerator.parser;

import java.util.*;

public class PDDLParser {

    public PDDLParser() { }

    public Map<Float, List<String>> getPDDLPlan(String plannerOutput) {
        Map<Float, List<String>> pddlPlan = new TreeMap<>();

        String[] output = plannerOutput.split("\n");
        for(String row : output) {
            if (isPlanRow(row)) {
                Float step = Float.parseFloat(row.split(":")[0]);

                int startActionIndex = row.indexOf("(") + 1;
                int endActionIndex = row.indexOf(")");

                if (!pddlPlan.containsKey(step))
                    pddlPlan.put(step, new ArrayList<>());

                List<String> actionList = pddlPlan.get(step);
                actionList.add(row.substring(startActionIndex, endActionIndex));
                pddlPlan.put(step, actionList);
            }
        }

        return pddlPlan;
    }

    private boolean isPlanRow(String row) {
        return !row.startsWith(";");
    }
}
