package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model;

import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.diagram.BPMNDiagram;

public class BPMN {

    private BPMNDefinitions definitions;
    private BPMNProcess process;
    private BPMNDiagram bpmnDiagram;

    public BPMNDefinitions getDefinitions() {
        return definitions;
    }

    public void setDefinitions(BPMNDefinitions definitions) {
        this.definitions = definitions;
    }

    public void setProcess(BPMNProcess process) {
        this.process = process;
    }

    public BPMNProcess getProcess() {
        return process;
    }

    public void setDiagram(BPMNDiagram bpmnDiagram) {
        this.bpmnDiagram = bpmnDiagram;
    }

    public BPMNDiagram getDiagram() {
        return bpmnDiagram;
    }
}
