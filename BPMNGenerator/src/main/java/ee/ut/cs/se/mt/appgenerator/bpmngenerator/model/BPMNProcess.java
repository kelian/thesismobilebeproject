package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BPMNProcess {

    private Map<String, String> attributes = new HashMap<>();

    private BPMNTask startEvent;
    private BPMNTask endEvent;

    private List<BPMNTask> bpmnTasks = new ArrayList<>();
    private List<SequenceFlow> sequenceFlows = new ArrayList<>();

    public BPMNProcess() {

    }

    public BPMNProcess(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setBpmnTasks(List<BPMNTask> bpmnTasks) {
        this.bpmnTasks = bpmnTasks;
    }

    public void addBpmnTask(BPMNTask bpmnTask) {
        bpmnTasks.add(bpmnTask);
    }

    public List<BPMNTask> getBpmnTasks() {
        return bpmnTasks;
    }

    public void setSequenceFlows(List<SequenceFlow> sequenceFlows) {
        this.sequenceFlows = sequenceFlows;
    }

    public void addSequenceFlow(SequenceFlow sequenceFlow) {
        sequenceFlows.add(sequenceFlow);
    }

    public List<SequenceFlow> getSequenceFlows() {
        return sequenceFlows;
    }

    public void setEndEvent(BPMNTask endEvent) {
        this.endEvent = endEvent;
    }

    public BPMNTask getEndEvent() {
        return endEvent;
    }

    public void setStartEvent(BPMNTask startEvent) {
        this.startEvent = startEvent;
    }

    public BPMNTask getStartEvent() {
        return startEvent;
    }
}
