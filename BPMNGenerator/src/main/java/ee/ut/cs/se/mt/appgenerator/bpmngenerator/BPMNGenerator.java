package ee.ut.cs.se.mt.appgenerator.bpmngenerator;

import ee.ut.cs.se.mt.appgenerator.bpmngenerator.generator.PlanBPMNFileGenerator;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMN;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.parser.PDDLParser;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.parser.PlanActionsBPMNParser;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.generator.BPMNXMLWriter;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;

import java.util.List;
import java.util.Map;

public class BPMNGenerator {

    public BPMNGenerator() { }

    public String create(String plannerOutput, ApplicationTemplate applicationTemplate) throws Exception {
        System.out.println(System.currentTimeMillis() + " - BPMN Generator: Start");

        Map<Float, List<String>> pddlPlan = new PDDLParser().getPDDLPlan(plannerOutput);
        System.out.println(System.currentTimeMillis() + " - BPMN Generator: PDDL plan is parsed");
        System.out.println(pddlPlan);

        Map<Float, List<BPMN>> actionsList = new PlanActionsBPMNParser().getActionsBPMN(applicationTemplate, pddlPlan);
        System.out.println(System.currentTimeMillis() + " - BPMN Generator: Mapped PDDL actions to BPMN actions");
        System.out.println(actionsList);

        BPMN planBPMN = new PlanBPMNFileGenerator().create(applicationTemplate, actionsList);
        System.out.println(System.currentTimeMillis() + " - BPMN Generator: Generated BPMN");

        String bpmn = new BPMNXMLWriter(planBPMN).create();
        System.out.println(System.currentTimeMillis() + " - BPMN Generator: Wrote BPMN into a file");

        return bpmn;
    }
}