package ee.ut.cs.se.mt.appgenerator.bpmngenerator.generator;

import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMN;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMNProcess;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMNTask;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.SequenceFlow;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.diagram.*;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import static ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.BPMNUtils.formatXML;
import static ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.BPMNUtils.writeBPMNFile;
import static ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.Constants.*;

public class BPMNXMLWriter {

    private BPMN bpmn;

    public BPMNXMLWriter(BPMN bpmn) {
        this.bpmn = bpmn;
    }

    public String create() throws Exception {
        XMLOutputFactory xmlOutFac = XMLOutputFactory.newInstance();
        StringWriter sw = new StringWriter();
        XMLStreamWriter writer = xmlOutFac.createXMLStreamWriter(sw);

        writeStartDocument(writer);
        writeDefinitions(writer);

        writer.flush();
        writer.close();

        String bpmnText = formatXML(sw.toString());
        writeBPMNFile("bpmn", bpmnText);

        sw.flush();

        return bpmnText;
    }

    private void writeStartDocument(XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartDocument("UTF-8", "1.0");
    }

    private void writeDefinitions(XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartElement(BPMN_DEFINITIONS);

        writeDefinitionsNamespace(writer);
        writeDefinitionsAttributes(writer);

        writeProcess(writer);
        writeDiagram(writer);

        writer.writeEndElement();
    }

    private void writeDefinitionsNamespace(XMLStreamWriter writer) throws XMLStreamException {
        Map<String, String> nameSpaceList = bpmn.getDefinitions().getNamespaceList();
        for (String key : nameSpaceList.keySet()) {
            writer.writeNamespace(key, nameSpaceList.get(key));
        }
    }

    private void writeDefinitionsAttributes(XMLStreamWriter writer) throws XMLStreamException {
        Map<String, String> attributeList = bpmn.getDefinitions().getAttributes();
        for (String key : attributeList.keySet()) {
            writer.writeAttribute(key, attributeList.get(key));
        }
    }

    private void writeProcess(XMLStreamWriter writer) throws XMLStreamException {
        if (bpmn.getProcess() != null) {
            writer.writeStartElement(BPMN_PROCESS);

            BPMNProcess process = bpmn.getProcess();

            writeAttributes(writer, process.getAttributes());
            writeProcessEvents(writer, process);
            writeSequenceFlows(writer, process.getSequenceFlows());

            writer.writeEndElement();
        }
    }

    private void writeProcessEvents(XMLStreamWriter writer, BPMNProcess process) throws XMLStreamException {
        writeProcessTask(writer, process.getStartEvent());
        writeProcessTask(writer, process.getEndEvent());

        if (process.getBpmnTasks() != null) {
            for (BPMNTask task : process.getBpmnTasks()) {
                writeProcessTask(writer, task);
            }
        }
    }

    private void writeProcessTask(XMLStreamWriter writer, BPMNTask task) throws XMLStreamException {
        writer.writeStartElement(task.getType());

        writer.writeAttribute(BPMN_ITEM_ID, task.getId());
        writeAttributes(writer, task.getAttributes());

        writer.writeEndElement();
    }

    private void writeAttributes(XMLStreamWriter writer, Map<String, String> attributes) throws XMLStreamException {
        if (attributes != null) {
            for (String key : attributes.keySet()) {
                writer.writeAttribute(key, attributes.get(key));
            }
        }
    }

    private void writeSequenceFlows(XMLStreamWriter writer, List<SequenceFlow> sequenceFlows) throws XMLStreamException {
        if (sequenceFlows != null) {
            for (SequenceFlow sequenceFlow : sequenceFlows) {
                writer.writeStartElement(BPMN_SEQUENCE_FLOW);

                writer.writeAttribute(BPMN_ITEM_ID, sequenceFlow.getId());
                writer.writeAttribute(BPMN_ITEM_SOURCE_REF, sequenceFlow.getSourceRef());
                writer.writeAttribute(BPMN_ITEM_TARGET_REF, sequenceFlow.getTargetRef());

                writer.writeEndElement();
            }
        }
    }

    private void writeDiagram(XMLStreamWriter writer) throws XMLStreamException {
        BPMNDiagram diagram = bpmn.getDiagram();
        if (diagram != null) {
            writer.writeStartElement(BPMN_PREFIX + BPMN_DIAGRAM);

            writer.writeAttribute(BPMN_ITEM_DOCUMENTATION, diagram.getDocumentation());
            writer.writeAttribute(BPMN_ITEM_ID, diagram.getId());
            writer.writeAttribute(BPMN_ITEM_NAME, diagram.getName());

            writePlane(writer, diagram.getBpmnPlane());

            writer.writeEndElement();
        }
    }

    private void writePlane(XMLStreamWriter writer, BPMNPlane plane) throws XMLStreamException {
        if (plane != null) {
            writer.writeStartElement(BPMN_PREFIX + BPMN_PLANE);

            writer.writeAttribute(BPMN_ITEM_ELEMENT, plane.getBpmnElement());

            writeShapes(writer, plane.getBpmnShapeList());
            writeEdges(writer, plane.getBpmnEdgeList());

            writer.writeEndElement();
        }
    }

    private void writeShapes(XMLStreamWriter writer, List<BPMNShape> shapes) throws XMLStreamException {
        if (shapes != null) {
            for (BPMNShape bpmnShape: shapes) {
                writer.writeStartElement(BPMN_PREFIX + BPMN_SHAPE);

                writer.writeAttribute(BPMN_ITEM_ELEMENT, bpmnShape.getBpmnElement());
                writer.writeAttribute(BPMN_ITEM_ID, bpmnShape.getId());

                writeBounds(writer, bpmnShape.getBounds());
                writeLabel(writer, bpmnShape.getBpmnLabel());

                writer.writeEndElement();
            }
        }
    }

    private void writeEdges(XMLStreamWriter writer, List<BPMNEdge> edges) throws XMLStreamException {
        if (edges != null) {
            for (BPMNEdge edge : edges) {
                writer.writeStartElement(BPMN_PREFIX + BPMN_EDGE);

                writer.writeAttribute(BPMN_ITEM_ELEMENT, edge.getBpmnElement());
                writer.writeAttribute(BPMN_ITEM_ID, edge.getId());
                writer.writeAttribute(BPMN_ITEM_ELEMENT_SOURCE, edge.getSourceElement());
                writer.writeAttribute(BPMN_ITEM_ELEMENT_TARGET, edge.getTargetElement());

                writeWaypoints(writer, edge.getWaypointList());
                writeLabel(writer, edge.getBpmnLabel());

                writer.writeEndElement();
            }
        }
    }

    private void writeLabel(XMLStreamWriter writer, BPMNLabel label) throws XMLStreamException {
        if (label != null) {
            writer.writeStartElement(BPMN_PREFIX + BPMN_LABEL);

            writeBounds(writer, label.getBounds());

            writer.writeEndElement();
        }
    }

    private void writeBounds(XMLStreamWriter writer, Bounds bounds) throws XMLStreamException {
        if (bounds != null) {
            writer.writeStartElement("omgdc:" + BPMN_BOUNDS);

            writer.writeAttribute(BPMN_ITEM_HEIGHT, bounds.getHeight());
            writer.writeAttribute(BPMN_ITEM_WIDTH, bounds.getWidth());
            writer.writeAttribute(BPMN_ITEM_X, bounds.getX());
            writer.writeAttribute(BPMN_ITEM_Y, bounds.getY());

            writer.writeEndElement();
        }
    }

    private void writeWaypoints(XMLStreamWriter writer, List<Waypoint> waypoints) throws XMLStreamException {
        if (waypoints != null) {
            for (Waypoint waypoint: waypoints) {
                writer.writeStartElement("omgdi:" + BPMN_WAYPOINT);

                writer.writeAttribute(BPMN_ITEM_X, waypoint.getX());
                writer.writeAttribute(BPMN_ITEM_Y, waypoint.getY());

                writer.writeEndElement();
            }
        }
    }

}