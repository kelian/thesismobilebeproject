package ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils;

public class Constants {

    // BPMN items
    public static final String BPMN_PREFIX = "bpmndi:";

    public static final String BPMN_DEFINITIONS = "definitions";
    public static final String BPMN_PROCESS = "process";
    public static final String BPMN_DIAGRAM = "BPMNDiagram";
    public static final String BPMN_PLANE = "BPMNPlane";
    public static final String BPMN_SHAPE = "BPMNShape";
    public static final String BPMN_EDGE = "BPMNEdge";
    public static final String BPMN_LABEL = "BPMNLabel";
    public static final String BPMN_BOUNDS = "Bounds";
    public static final String BPMN_WAYPOINT = "waypoint";

    public static final String BPMN_SEQUENCE_FLOW = "sequenceFlow";

    public static final String BPMN_EVENT_START = "startEvent";
    public static final String BPMN_EVENT_END = "endEvent";

    public static final String BPMN_ITEM_ID = "id";
    public static final String BPMN_ITEM_NAME = "name";
    public static final String BPMN_ITEM_SOURCE_REF = "sourceRef";
    public static final String BPMN_ITEM_TARGET_REF = "targetRef";
    public static final String BPMN_ITEM_DOCUMENTATION = "documentation";
    public static final String BPMN_ITEM_ELEMENT = "bpmnElement";
    public static final String BPMN_ITEM_ELEMENT_SOURCE = "sourceElement";
    public static final String BPMN_ITEM_ELEMENT_TARGET = "targetElement";
    public static final String BPMN_ITEM_HEIGHT = "height";
    public static final String BPMN_ITEM_WIDTH = "width";
    public static final String BPMN_ITEM_X = "x";
    public static final String BPMN_ITEM_Y = "y";

}
