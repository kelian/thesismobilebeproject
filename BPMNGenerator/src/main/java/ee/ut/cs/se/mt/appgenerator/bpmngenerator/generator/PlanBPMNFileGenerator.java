package ee.ut.cs.se.mt.appgenerator.bpmngenerator.generator;

import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMN;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMNProcess;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.BPMNTask;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.SequenceFlow;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.diagram.*;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.parser.BPMNParser;
import ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.BPMNUtils;
import ee.ut.cs.se.mt.appgenerator.model.ApplicationTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.Constants.BPMN_ITEM_ID;
import static ee.ut.cs.se.mt.appgenerator.bpmngenerator.utils.Constants.BPMN_ITEM_NAME;

public class PlanBPMNFileGenerator {

    private BPMN planBPMN = new BPMN();
    private BPMN baseBPMN;

    private int planBPMNLastId = 4; // Because startEvent is 2 and endEvent is 3.

    private Bounds planBPMNLastBounds;
    private Bounds currentLastBounds;

    private String planLastElement;
    private String baseEndElement;

    float currentMaxY = 0f;

    public PlanBPMNFileGenerator() { }

    public BPMN create(ApplicationTemplate applicationTemplate, Map<Float, List<BPMN>> actionBPMNList) throws Exception {
        this.baseBPMN = new BPMNParser().getBPMN(BPMNUtils.getEmptyBPMNFile());

        planBPMN.setDefinitions(baseBPMN.getDefinitions());
        generateBody(applicationTemplate.getNameWithRequirements(), applicationTemplate.getIdWithRequirements(), actionBPMNList);

        return planBPMN;
    }

    private void generateBody(String appName, String appId, Map<Float, List<BPMN>> actionBPMNList) {
        generateProcess(appName, appId);
        generateDiagram(appId);

        addStartEventToDiagram();
        addItems(actionBPMNList);
    }

    private void generateProcess(String appName, String appId) {
        BPMNProcess planProcess = new BPMNProcess(getProcessAttributes(appName, appId));
        planProcess.setStartEvent(baseBPMN.getProcess().getStartEvent());
        planProcess.setEndEvent(baseBPMN.getProcess().getEndEvent());

        planLastElement = planProcess.getStartEvent().getId();
        baseEndElement = planProcess.getEndEvent().getId();

        planBPMN.setProcess(planProcess);
    }

    private Map<String, String> getProcessAttributes(String appName, String appId) {
        Map<String, String> attributes = baseBPMN.getProcess().getAttributes();
        attributes.put(BPMN_ITEM_ID, appId);
        attributes.put(BPMN_ITEM_NAME, appName);

        return attributes;
    }

    private void generateDiagram(String appId) {
        BPMNDiagram baseDiagram = baseBPMN.getDiagram();
        BPMNDiagram diagram = new BPMNDiagram(baseDiagram.getDocumentation(), baseDiagram.getId(), baseDiagram.getName());
        planBPMN.setDiagram(diagram);

        generatePlane(appId);
    }

    private void generatePlane(String appId) {
        BPMNPlane plane = new BPMNPlane(appId);

        planBPMN.getDiagram().setBpmnPlane(plane);
    }

    private void addStartEventToDiagram() {
        BPMNShape baseShape = baseBPMN.getDiagram().getBpmnPlane().getBpmnShapeList().get(0);
        planBPMN.getDiagram().getBpmnPlane().addBpmnShape(baseShape);

        planBPMNLastBounds = baseShape.getBounds();
        currentLastBounds = planBPMNLastBounds;
    }

    private void addItems(Map<Float, List<BPMN>> actionBPMNList) {
        for (Float step : actionBPMNList.keySet()) {
            List<BPMN> bpmnList = actionBPMNList.get(step);

            if (bpmnList.size() == 1) {
                addBPMNItems(bpmnList.get(0), 0f);
            } else {
                addParallelItems(bpmnList);
            }
        }

        addEndSequenceFlow();
        addEndShape();
        addEdges();
    }

    private void addParallelItems(List<BPMN> bpmnList) {
        String parallelGatewayId = "_" + planBPMNLastId;
        planBPMNLastId += 1;

        addParallelGateway(parallelGatewayId);

        float extraY = 0f;
        List<String> lastElementsList = new ArrayList<>();

        for (BPMN bpmn : bpmnList) {
            planLastElement = parallelGatewayId;
            addBPMNItems(bpmn, extraY);

            extraY = currentMaxY;
            lastElementsList.add(planLastElement);
        }

        addLastParallelGateway(lastElementsList);
    }

    private void addParallelGateway(String parallelGatewayId) {
        addParallelTask(parallelGatewayId);
        addParallelSequenceFlow(parallelGatewayId, planLastElement);
        addParallelShape(parallelGatewayId);
    }

    private void addLastParallelGateway(List<String> lastElementsList) {
        String parallelGatewayId = "_" + planBPMNLastId;
        planBPMNLastId += 1;

        addParallelTask(parallelGatewayId);
        addParallelShape(parallelGatewayId);

        for (String id : lastElementsList) {
            addParallelSequenceFlow(parallelGatewayId, id);
        }
    }

    private void addParallelTask(String parallelGatewayId) {
        BPMNTask parallelTask = new BPMNTask("parallelGateway");
        parallelTask.addAttribute("name", "ParallelGateway");
        parallelTask.setId(parallelGatewayId);

        planBPMN.getProcess().addBpmnTask(parallelTask);
    }

    private void addParallelSequenceFlow(String parallelGatewayId, String sourceRef) {
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setId("_" + planBPMNLastId);
        planBPMNLastId += 1;

        sequenceFlow.setTargetRef(parallelGatewayId);
        sequenceFlow.setSourceRef(sourceRef);

        planLastElement = parallelGatewayId;

        planBPMN.getProcess().addSequenceFlow(sequenceFlow);
    }

    private void addParallelShape(String parallelGatewayId) {
        BPMNShape shape = new BPMNShape();

        shape.setBpmnElement(parallelGatewayId);
        shape.setId("Shape-" + parallelGatewayId);

        Bounds bounds = new Bounds();
        bounds.setHeight("32.0");
        bounds.setWidth("32.0");

        bounds.setX(planBPMNLastBounds.getXFloat() + planBPMNLastBounds.getWidthFloat() + 100);
        bounds.setY(planBPMNLastBounds.getY());

        planBPMNLastBounds = bounds;

        shape.setBounds(bounds);

        shape.setBpmnLabel(new BPMNLabel());
        shape.getBpmnLabel().setBounds(new Bounds("32.0", "32.0", "0", "0"));

        planBPMN.getDiagram().getBpmnPlane().addBpmnShape(shape);
    }

    private void addBPMNItems(BPMN bpmn, float extraY) {
        String currentStartElement = bpmn.getProcess().getStartEvent().getId();
        String currentEndElement = bpmn.getProcess().getEndEvent().getId();

        int taskExtraId = planBPMNLastId - getMinimumTaskId(bpmn.getProcess().getBpmnTasks());
        addTask(bpmn.getProcess().getBpmnTasks(), taskExtraId);

        int sequenceExtraId = planBPMNLastId - getMinimumSequenceId(bpmn.getProcess().getSequenceFlows());
        addSequenceFlows(bpmn.getProcess(), taskExtraId, sequenceExtraId, currentStartElement, currentEndElement);

        addElements(bpmn, extraY, taskExtraId, currentStartElement, currentEndElement);

        planBPMNLastBounds = currentLastBounds;
    }

    private void addTask(List<BPMNTask> bpmnTasks, int taskExtraId) {
        for (BPMNTask task : bpmnTasks) {
            task.setId(getNewId(task.getId(), taskExtraId));

            planBPMN.getProcess().addBpmnTask(task);
        }
    }

    private void addSequenceFlows(BPMNProcess process, int taskExtraId, int sequenceExtraId, String currentStartElement,
                                  String currentEndElement) {

        for (SequenceFlow sequenceFlow : process.getSequenceFlows()) {
            sequenceFlow.setId(getNewId(sequenceFlow.getId(), sequenceExtraId));
            addSequenceSourceRef(sequenceFlow, currentStartElement, taskExtraId);

            if (!sequenceFlow.getTargetRef().equals(currentEndElement)) {
                addSequenceTargetRef(sequenceFlow, taskExtraId);
                planBPMN.getProcess().addSequenceFlow(sequenceFlow);
            } else {
                planLastElement = sequenceFlow.getSourceRef();
            }
        }
    }

    private void addSequenceSourceRef(SequenceFlow sequenceFlow, String currentStartElement, int taskExtraId) {
        if (sequenceFlow.getSourceRef().equals(currentStartElement)) {
            sequenceFlow.setSourceRef(planLastElement);

        } else {
            String newSourceId = getNewId(sequenceFlow.getSourceRef(), taskExtraId);
            sequenceFlow.setSourceRef(newSourceId);
        }
    }

    private void addSequenceTargetRef(SequenceFlow sequenceFlow, int taskExtraId) {
        String newTargetId = getNewId(sequenceFlow.getTargetRef(), taskExtraId);
        sequenceFlow.setTargetRef(newTargetId);
    }

    private void addElements(BPMN bpmn, float extraY, int taskExtraId, String currentStartElement, String currentEndElement) {
        for (BPMNShape shape : bpmn.getDiagram().getBpmnPlane().getBpmnShapeList()) {
            if (notStartOrLastElement(shape.getBpmnElement(), currentStartElement, currentEndElement)) {
                float extraValue = planBPMNLastBounds.getXFloat() + 5; // extra space;

                String newElement = getNewId(shape.getBpmnElement(), taskExtraId);
                String newId = "Shape-" + newElement;

                shape.setBpmnElement(newElement);
                shape.setId(newId);

                float currentX = Float.parseFloat(shape.getBounds().getX());
                float newX = currentX + extraValue;
                shape.getBounds().setX(String.valueOf(newX));
                shape.getBounds().setY(shape.getBounds().getYFloat() + extraY);

                planBPMN.getDiagram().getBpmnPlane().addBpmnShape(shape);

                if (shape.getBounds().getXFloat() > currentLastBounds.getXFloat()) {
                    currentLastBounds = shape.getBounds();
                }

                if (shape.getBounds().getYFloat() > currentMaxY) {
                    currentMaxY = shape.getBounds().getYFloat();
                }
            }
        }
    }

    private void addEdges() {
        for (SequenceFlow sequenceFlow : planBPMN.getProcess().getSequenceFlows()) {
            BPMNEdge edge = new BPMNEdge();
            edge.setBpmnElement(sequenceFlow.getId());
            edge.setId("BPMNEdge_" + sequenceFlow.getId());
            edge.setSourceElement(sequenceFlow.getSourceRef());
            edge.setTargetElement(sequenceFlow.getTargetRef());

            setStartWayPoint(edge, edge.getSourceElement());
            setEndWayPoint(edge, edge.getTargetElement());

            edge.setBpmnLabel(baseBPMN.getDiagram().getBpmnPlane().getBpmnEdgeList().get(0).getBpmnLabel());

            planBPMN.getDiagram().getBpmnPlane().addBPMNEdge(edge);
        }
    }

    private void setStartWayPoint(BPMNEdge edge, String sourceElement) {
        BPMNShape shape = getShapeById(sourceElement);

        if (shape != null) {
            Waypoint waypoint = new Waypoint();

            Float x = shape.getBounds().getWidthFloat() + shape.getBounds().getXFloat();
            Float y = (shape.getBounds().getHeightFloat() / 2) + shape.getBounds().getYFloat();

            waypoint.setX(String.valueOf(x));
            waypoint.setY(String.valueOf(y));

            edge.addWaypoint(waypoint);
        }
    }

    private void setEndWayPoint(BPMNEdge edge, String targetElement) {
        BPMNShape shape = getShapeById(targetElement);

        if (shape != null) {
            Waypoint waypoint = new Waypoint();

            Float x = shape.getBounds().getXFloat();
            Float y = (shape.getBounds().getHeightFloat() / 2) + shape.getBounds().getYFloat();

            waypoint.setX(String.valueOf(x));
            waypoint.setY(String.valueOf(y));

            edge.addWaypoint(waypoint);
        }
    }

    private BPMNShape getShapeById(String sourceElement) {
        for (BPMNShape shape : planBPMN.getDiagram().getBpmnPlane().getBpmnShapeList()) {
            if (shape.getBpmnElement().equals(sourceElement)) {
                return shape;
            }
        }
        return null;
    }

    private void addEndSequenceFlow() {
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setId("_" + (planBPMNLastId + 1));

        sequenceFlow.setSourceRef(planLastElement);
        sequenceFlow.setTargetRef(baseEndElement);

        planBPMN.getProcess().addSequenceFlow(sequenceFlow);
    }

    private void addEndShape() {
        float x = planBPMNLastBounds.getXFloat() + planBPMNLastBounds.getWidthFloat() + 100;

        BPMNShape baseLastShape = baseBPMN.getDiagram().getBpmnPlane().getBpmnShapeList().get(1);
        baseLastShape.getBounds().setX(x);

        planBPMN.getDiagram().getBpmnPlane().addBpmnShape(baseLastShape);
    }

    private int getMinimumTaskId(List<BPMNTask> bpmnTasks) {
        int minimumId = getId(bpmnTasks.get(0).getId());

        for (BPMNTask task : bpmnTasks) {
            int id = getId(task.getId());

            if (id < minimumId) {
                minimumId = id;
            }
        }

        return minimumId;
    }

    private int getMinimumSequenceId(List<SequenceFlow> sequenceFlows) {
        int minimumId = getId(sequenceFlows.get(0).getId());

        for (SequenceFlow sequenceFlow : sequenceFlows) {
            int id = getId(sequenceFlow.getId());

            if (id < minimumId) {
                minimumId = id;
            }
        }

        return minimumId;
    }

    private int getId(String id) {
        return Integer.parseInt(id.substring(1));
    }

    private String getNewId(String id, int extraId) {
        int idNr = getId(id) + extraId;

        if ((idNr + 1) > planBPMNLastId) {
            planBPMNLastId = (idNr + 1);
        }

        return "_" + idNr;
    }

    private boolean notStartOrLastElement(String id, String currentStartElement, String currentEndElement) {
        return !id.equals(currentStartElement) && !id.equals(currentEndElement);
    }
}
