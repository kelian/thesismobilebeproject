package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model;

import java.util.HashMap;
import java.util.Map;

public class BPMNTask {

    private String type;
    private String id;

    private Map<String, String> attributes = new HashMap<>();

    public BPMNTask(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void addAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
