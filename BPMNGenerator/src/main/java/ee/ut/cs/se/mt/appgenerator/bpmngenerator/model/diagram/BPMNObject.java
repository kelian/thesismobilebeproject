package ee.ut.cs.se.mt.appgenerator.bpmngenerator.model.diagram;

public class BPMNObject {

    private BPMNLabel bpmnLabel;

    public void setBpmnLabel(BPMNLabel bpmnLabel) {
        this.bpmnLabel = bpmnLabel;
    }

    public BPMNLabel getBpmnLabel() {
        return bpmnLabel;
    }
}